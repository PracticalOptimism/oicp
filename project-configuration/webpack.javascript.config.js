
const webpackCommonConfig = require('./webpack.common.config')

module.exports = {
  ...webpackCommonConfig,
  target: 'web',
  mode: 'development',
  entry: {
    javascript: './precompiled/javascript.ts',
  }
}

