
const webpackCommonConfig = require('./webpack.common.config')

module.exports = {
  ...webpackCommonConfig,
  target: 'node',
  mode: 'development',
  entry: {
    'http-server': './precompiled/http-server.ts'
  }
}

