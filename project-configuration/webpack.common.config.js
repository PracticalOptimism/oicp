
const path = require('path')

module.exports = {
  mode: 'development',
  output: {
    filename: '[name].js',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, '../compiled')
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    alias: {
      'precompiled': path.resolve(__dirname, '../precompiled'),
      '@': path.resolve(__dirname, '../precompiled')
    }
  },
  devServer: {
    contentBase: path.join(__dirname, '../compiled'),
    port: 8080,
    host: `localhost`,
  },
  devtool: false
}

