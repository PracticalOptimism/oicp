


class InternetUser {
  internetUserId: string = ''
  asymmetricCryptographyPublicKey: string = ''

  isNowOnline: boolean = false
  dateLastOnline?: Date

  physicalLocationAddress: string = ''
  internetServiceProvider: string = ''

  connectionDataChannelCredentialTable: { [dataChannelId: string]: InternetConnectionDataChannelCredential } = {}

  connectionTable: { [internetConnectionId: string]: InternetConnection } = {}

  messageTable: { [messageId: string]: InternetMessage } = {}

  dateCreated: Date = new Date()
}

class InternetConnection {
  internetConnectionId: string = ''
}

class InternetConnectionDataChannelCredential {
  internetConnectionDataChannelCredentialId: string = ''

  // alternative name: network protocol id
  dataChannelId: string = 'http' // http, webrtc, websocket
  dataChannelCredential: any
}

class InternetMessage {
  internetMessageId: string = ''

  senderInternetUserId: string = ''
  recipientInternetUserId: string = ''

  dateCreated: Date = new Date()

  message: any
}


export {
  InternetUser,
  InternetConnection,
  InternetConnectionDataChannelCredential,
  InternetMessage
}
