
import * as _project from '../../../../@project/@architecture'
import * as usecase from '../../../../usecase/@architecture'

const JAVASCRIPT_LIBRARY_INSTANCE_MODEL = {
  _project,
  consumer: {
    javascript: {},
    httpServer: {}
  },
  usecase,
  provider: {}
}

export {
  JAVASCRIPT_LIBRARY_INSTANCE_MODEL
}

