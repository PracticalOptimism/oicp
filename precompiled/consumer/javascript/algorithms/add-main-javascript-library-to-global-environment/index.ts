
import { javascriptGlobalVariable, mainJavascriptLibrary } from '../../variables/javascript'
import { PROJECT_JAVASCRIPT_LIBRARY_ID } from '../../../../@project/constants/project'

// Algorithms

function addMainJavascriptLibraryToGlobalEnvironmentFunction () {
  if (javascriptGlobalVariable) {
    (javascriptGlobalVariable as any)[PROJECT_JAVASCRIPT_LIBRARY_ID] = mainJavascriptLibrary
  }
}

// Constants

const addMainJavascriptLibraryToGlobalEnvironment = {
  function: addMainJavascriptLibraryToGlobalEnvironmentFunction
}

export {
  addMainJavascriptLibraryToGlobalEnvironment
}

