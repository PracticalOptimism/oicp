
const javascriptGlobalVariable = this || global || globalThis || window

const mainJavascriptLibrary = { consumer: { javascript: {} } }

export {
  javascriptGlobalVariable,
  mainJavascriptLibrary
}
