
import { httpServerTable } from '../../variables/http-server'

function deleteHttpServerFunction (httpServerId: string): boolean {

  const httpServer = httpServerTable[httpServerId]

  if (!httpServer) {
    throw Error(`Cannot find http server by httpServerId: ${httpServerId}`)
  }

  delete httpServerTable[httpServerId]

  return true
}

const deleteHttpServer = {
  function: deleteHttpServerFunction
}

export {
  deleteHttpServer
}

