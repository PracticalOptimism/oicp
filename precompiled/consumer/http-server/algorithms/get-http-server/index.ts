
import { HttpServer } from '../../data-structures/http-server'
import { httpServerTable } from '../../variables/http-server'

function getHttpServerFunction (httpServerId: string): HttpServer {
  return httpServerTable[httpServerId]
}

const getHttpServer = {
  function: getHttpServerFunction
}

export {
  getHttpServer
}
