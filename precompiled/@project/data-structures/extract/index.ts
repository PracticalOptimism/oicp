
class ProjectExtract {
  usecase?: ProjectUsecaseExtract = new ProjectUsecaseExtract()

  constructor (defaultOption?: ProjectExtract) {
    Object.assign(this, defaultOption)
  }
}

class ProjectUsecaseExtract {
  constructor (defaultOption?: ProjectUsecaseExtract) {
    Object.assign(this, defaultOption)
  }
}

export {
  ProjectExtract,
  ProjectUsecaseExtract
}

