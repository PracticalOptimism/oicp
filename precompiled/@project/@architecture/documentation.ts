
import * as constantsDocumentation from '../constants/@architecture/documentation'
import * as dataStructuresDocumentation from '../data-structures/@architecture/documentation'


export {
  constantsDocumentation,
  dataStructuresDocumentation
}
